class riseupsites {

  include riseuplabs::site

  include site_apache::wildcard
  apache::module {
    'removeip': ensure => present, package_name => 'libapache2-mod-removeip';
    'rewrite':  ensure => present;
    'headers':  ensure => present;
    'dir':  ensure => present;
    'alias':  ensure => present;
    'authz_host':  ensure => present;
    'authz_core':  ensure => present;
    'socache_shmcb':  ensure => present;
    'mime':  ensure => present;
    'mpm_prefork': ensure => present;

  }

  apache::vhost::file {
    # for mail auto-configuration (see: https://wiki.mozilla.org/Thunderbird:Autoconfiguration:ConfigFileFormat
    # and https://developer.mozilla.org/en/Thunderbird/Autoconfiguration/FileFormat/HowTo)
    'autoconfig':
      content => template('riseupsites/kiwi/autoconfig.riseup.net');
    'status':
      content => template('riseupsites/kiwi/status.riseup.net');
    'mta-sts':
      content => template('riseupsites/kiwi/mta-sts.riseup.net');
    'labs':
      content => template('riseupsites/kiwi/labs.riseup.net');
  }

  file {

    # the following is for the mail autoconfiguration
    '/var/www/autoconfig':
      ensure => directory,
      owner  => www-data,
      group  => www-data,
      mode   => '0755';

    '/var/www/autoconfig/mail':
      ensure => directory,
      owner  => www-data,
      group  => www-data,
      mode   => '0755';

    '/var/www/autoconfig/riseup.net.xml':
      source => 'puppet:///modules/riseupsites/autoconfig/riseup.net.xml',
      owner  => www-data,
      group  => www-data,
      mode   => '0444';

    '/var/www/autoconfig/mail/config-v1.1.xml':
      source => 'puppet:///modules/riseupsites/autoconfig/config-v1.1.xml',
      owner  => www-data,
      group  => www-data,
      mode   => '0444';

    # https://tools.ietf.org/html/rfc8461
    '/var/www/mta-sts':
      ensure => directory,
      owner  => www-data,
      group  => www-data,
      mode   => '0755';

    '/var/www/mta-sts/.well-known':
      ensure => directory,
      owner  => www-data,
      group  => www-data,
      mode   => '0755';

    '/var/www/mta-sts/.well-known/mta-sts.txt':
      source => 'puppet:///modules/riseupsites/mta-sts/mta-sts.txt',
      owner  => www-data,
      group  => www-data,
      mode   => '0444';

  }

}
